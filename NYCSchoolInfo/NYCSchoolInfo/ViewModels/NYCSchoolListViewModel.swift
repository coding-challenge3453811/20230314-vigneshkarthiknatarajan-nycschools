//
//  NYCSchoolListViewModel.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import Combine
import Foundation


/// School detail params
struct SchoolDetail: Identifiable {
    var id = UUID()
    var dbn = ""
    var name = ""
    var overviewParagraph = ""
    var academicopportunitiesOne = ""
    var academicopportunitiesTwo = ""
    var phoneNumber = ""
    var faxNumber = ""
    var schoolEmail = ""
    var website = ""
    var finalgrades = ""
    var totalStudents = ""
    var primaryAddress = ""
    var borough = ""
}


/// School SAT score params
struct SchoolSATScoreDetail {
    var dbn = ""
    var name = ""
    var numOfSatTestTakers = ""
    var readingAvgScore = ""
    var mathAvgScore = ""
    var writingAvgScore = ""
}


/// View modl class holds properties related to NYCSchoolListLandingView
class NYCSchoolListViewModel: ObservableObject {
    @Published var listOfSchools = [SchoolDetail]()
    @Published var isDataLoaded = false
    @Published var isAPIFailed = false
    var listOfSchoolSATScore = [SchoolSATScoreDetail]()
    var serviceHandler: NYCSchoolServiceHandler
    var responseHandler: NYCSchoolResponseHandler
    
    /// Initializes NYCSchoolListViewModel
    /// - Parameter serviceHandler: default param initializes NYCSchoolServiceHandler instance
    init(serviceHandler: NYCSchoolServiceHandler = NYCSchoolServiceHandler(),
         responseHandler: NYCSchoolResponseHandler = NYCSchoolResponseHandler()) {
        self.serviceHandler = serviceHandler
        self.responseHandler = responseHandler
    }
    
    /// Method used to fetch nyc school list
    func fetchSchoolListAPI() {
        if let url = URL(string: NYCSchoolConstants.URLString.schoolListUrl()) {
            serviceHandler.fetchData(url: url) {result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        self.responseHandler.mapData(data: response,
                                                responseType: [NYCSchoolListModel].self, completion: { result in
                            switch result {
                            case .success(let response):
                                self.isDataLoaded = true
                                self.handleSchoolListResponse(response: response)
                                self.fetchSchoolDetailsAPI()
                            case .failure(_):
                                self.isAPIFailed = true
                            }
                        })
                    case .failure(_):
                        self.isAPIFailed = true
                    }
                }
            }
        }
    }
    
    /// Method used to map viewmodel and response model
    /// - Parameter response: Array of NYCSchoolListModel
    func handleSchoolListResponse(response: [NYCSchoolListModel]) {
        listOfSchools = response.map { school -> SchoolDetail in
            let address = school.primaryAddressLine + ", " + school.city + " " + school.stateCode + " " + school.zip
            let schoolDetail = SchoolDetail(dbn: school.dbn,
                                name: school.schoolName,
                                overviewParagraph: school.overviewParagraph,
                                academicopportunitiesOne: school.academicopportunitiesOne ?? "",
                                academicopportunitiesTwo: school.academicopportunitiesTwo ?? "",
                                phoneNumber: school.phoneNumber,
                                faxNumber: school.faxNumber ?? "",
                                schoolEmail: school.schoolEmail ?? "",
                                website: school.website,
                                finalgrades: school.finalgrades,
                                totalStudents: school.totalStudents,
                                primaryAddress: address,
                                borough: school.borough ?? "")
            return schoolDetail
        }
    }
    
    /// Method used to fetch nyc school sat score list
    func fetchSchoolDetailsAPI() {
        if let url = URL(string: NYCSchoolConstants.URLString.schoolDetailUrl()) {
            serviceHandler.fetchData(url: url) { result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        self.responseHandler.mapData(data: response,
                                                responseType: [NYCSchoolDetailsModel].self, completion: { result in
                            switch result {
                            case .success(let response):
                                self.handleSchoolDetailsResponse(response: response)
                            case .failure(_):
                                self.isAPIFailed = true
                            }
                        })
                    case .failure(_):
                        self.isAPIFailed = true
                    }
                }
            }
        }
    }
    
    /// Method used to map viewmodel and response model
    /// - Parameter response: Array of NYCSchoolDetailsModel
    func handleSchoolDetailsResponse(response: [NYCSchoolDetailsModel]) {
        listOfSchoolSATScore = response.map { schoolSATScore -> SchoolSATScoreDetail in
            let schoolSATScoreDetail = SchoolSATScoreDetail(dbn: schoolSATScore.dbn,
                                                            name: schoolSATScore.schoolName,
                                                            numOfSatTestTakers: schoolSATScore.numOfSatTestTakers,
                                                            readingAvgScore: schoolSATScore.satCriticalReadingAvgScore,
                                                            mathAvgScore: schoolSATScore.satMathAvgScore,
                                                            writingAvgScore: schoolSATScore.satWritingAvgScore)
            return schoolSATScoreDetail
        }
    }
    
    /// Method returns sat score detail matcing for selected school
    /// - Parameter dbn: school DBN
    /// - Returns: School sat score
    func getSchoolSATScore(dbn: String) -> [SchoolSATScoreDetail]? {
        let satScore = listOfSchoolSATScore.filter({ $0.dbn == dbn })
        return satScore
    }
}
