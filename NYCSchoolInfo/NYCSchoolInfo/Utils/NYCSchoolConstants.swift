//
//  NYCSchoolConstants.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import Foundation


/// Constant values
struct NYCSchoolConstants {
    enum URLString: String {
        case schoolListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        case schoolDetailUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        
        func callAsFunction() -> String {
            self.rawValue
        }
    }
    
    enum NumericalValues: Double {
        case maxPadding = 16.0
        case minPadding = 8.0
        case cornerRadius = 4.0
        case ctaWidth = 100.0
        case ctaHeight = 44.0
        case dividerHeight = 1.0
        
        func callAsFunction() -> Double {
            self.rawValue
        }
    }
}
