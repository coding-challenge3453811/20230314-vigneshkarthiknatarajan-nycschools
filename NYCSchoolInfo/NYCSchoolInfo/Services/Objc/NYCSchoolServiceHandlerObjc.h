//
//  NYCSchoolServiceHandlerObjc.h
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/15/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NYCSchoolServiceHandlerObjc : NSObject

+ (void)fetchResponseDataFor:(NSURL *)url withCompletion:(void(^)(NSData * _Nullable data, NSError * _Nullable error))callback;

@end

NS_ASSUME_NONNULL_END
