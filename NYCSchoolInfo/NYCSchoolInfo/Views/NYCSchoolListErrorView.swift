//
//  NYCSchoolListErrorView.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import SwiftUI

/// NYCSchoolListErrorView - Displays error view
struct NYCSchoolListErrorView: View {
    @ObservedObject var schoolListViewModel: NYCSchoolListViewModel
    var body: some View {
        ZStack {
            Color("default_background")
            VStack(spacing: 10, content:  {
                Text("school_list_error_text")
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, NYCSchoolConstants.NumericalValues.maxPadding())
                    .padding(.vertical, NYCSchoolConstants.NumericalValues.maxPadding())
                Button(action: {
                    schoolListViewModel.fetchSchoolListAPI()
                }, label: {
                    Text("school_list_error_retry")
                        .padding(.vertical, NYCSchoolConstants.NumericalValues.maxPadding())
                })
                .padding(.horizontal, NYCSchoolConstants.NumericalValues.maxPadding())
                .padding(.vertical, NYCSchoolConstants.NumericalValues.maxPadding())
                .frame(width: NYCSchoolConstants.NumericalValues.ctaWidth(),
                       height: NYCSchoolConstants.NumericalValues.ctaHeight())
                .background(Color("splash_background"))
                .foregroundColor(Color("default_white"))
            })
        }
    }
}
