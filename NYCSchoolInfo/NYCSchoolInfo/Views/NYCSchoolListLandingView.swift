//
//  NYCSchoolListLandingView.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import SwiftUI

/// Destination - enum value for navigation path
enum Destination: Hashable {
    case details
}

/// NYCSchoolListLandingView - Display nyc school info 
struct NYCSchoolListLandingView: View {
    @StateObject private var schoolListViewModel = NYCSchoolListViewModel()
    @State private var showDetailView = false
    @State private var path: [Destination] = []
    @State private var selectedSchool = SchoolDetail()
    @State private var selectedSchoolSATScore = SchoolSATScoreDetail()
    @State private var showAlert = false
    
    var body: some View {
        NavigationStack(path: $path) {
            VStack(spacing: 0, content:  {
                if !schoolListViewModel.isDataLoaded {
                    NYCSchoolLoadingIndicatorView()
                } else if schoolListViewModel.isAPIFailed {
                    NYCSchoolListErrorView(schoolListViewModel: schoolListViewModel)
                } else {
                    NYCSchoolListView(schoolListViewModel: schoolListViewModel,
                                      selectedSchool: $selectedSchool,
                                      selectedSchoolSATScore: $selectedSchoolSATScore,
                                      showDetailView: $showDetailView,
                                      path: $path,
                                      showAlert: $showAlert)
                }
            })
            .background(Color("default_background"))
            .onAppear(perform: {
                schoolListViewModel.fetchSchoolListAPI()
            })
            .alert(isPresented: $showAlert) {
                Alert(title: Text("alert_title"),
                      message: Text("alert_message"),
                      dismissButton: .default(Text("alert_dismiss")))
            }
            .navigationBarTitle("navigation_title", displayMode: .inline)
            .navigationDestination(for: Destination.self) {
                switch $0 {
                case .details:
                    NYCSchoolDetailView(school: selectedSchool,
                                        schoolSATScore: selectedSchoolSATScore)
                }
            }
        }
    }
}
