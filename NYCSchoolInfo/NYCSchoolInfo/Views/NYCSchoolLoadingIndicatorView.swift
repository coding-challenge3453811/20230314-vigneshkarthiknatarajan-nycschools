//
//  NYCSchoolLoadingIndicatorView.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/15/23.
//

import SwiftUI

struct NYCSchoolLoadingIndicatorView: View {
    var body: some View {
        ZStack {
            Color("default_background")
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
        }
    }
}
