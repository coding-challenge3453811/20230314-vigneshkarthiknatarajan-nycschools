//
//  NYCSchoolListView.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import SwiftUI

/// NYCSchoolListView - Displays List of schools in NYC
struct NYCSchoolListView: View {
    @ObservedObject var schoolListViewModel: NYCSchoolListViewModel
    @Binding var selectedSchool: SchoolDetail
    @Binding var selectedSchoolSATScore: SchoolSATScoreDetail
    @Binding var showDetailView: Bool
    @Binding var path: [Destination]
    @Binding var showAlert: Bool

    /// NYCSchoolListView body
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            ScrollView(content: {
                VStack(spacing: 0, content: {
                    ForEach(schoolListViewModel.listOfSchools) { school in
                        Button(action: {
                            if let satScoreArr = schoolListViewModel.getSchoolSATScore(dbn: school.dbn),
                               let satScore = satScoreArr.first {
                                selectedSchool = school
                                selectedSchoolSATScore = satScore
                                path.append(.details)
                            } else {
                                showAlert = true
                            }
                        }, label: {
                            NYCSchoolListCellView(school: school)
                        })
                        .foregroundColor(Color("text_color_black"))
                    }
                })
                .background(Color("default_white"))
                .cornerRadius(NYCSchoolConstants.NumericalValues.cornerRadius())
            })
            .padding(.horizontal, NYCSchoolConstants.NumericalValues.maxPadding())
        })
    }
}

/// NYCSchoolListCellView - Display school name and address
struct NYCSchoolListCellView: View {
    var school: SchoolDetail
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            HStack(spacing: 0, content: {
                VStack(alignment: .leading, spacing: 0, content: {
                    Text(school.name)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.leading)
                        .lineLimit(2)
                        .padding(.top, NYCSchoolConstants.NumericalValues.maxPadding())
                        .padding(.leading, NYCSchoolConstants.NumericalValues.maxPadding())
                    Text(school.primaryAddress)
                        .multilineTextAlignment(.leading)
                        .lineLimit(2)
                        .padding(.vertical, NYCSchoolConstants.NumericalValues.maxPadding())
                        .padding(.leading, NYCSchoolConstants.NumericalValues.maxPadding())
                })
                Spacer()
                HStack(spacing: 5, content: {
                    Text("school_view_sat_score")
                    Image("chevron_right")
                        .padding(.trailing, NYCSchoolConstants.NumericalValues.maxPadding())
                })
            })
            Divider()
                .frame(height: NYCSchoolConstants.NumericalValues.dividerHeight())
                .padding(.leading, NYCSchoolConstants.NumericalValues.maxPadding())
        })
    }
}

