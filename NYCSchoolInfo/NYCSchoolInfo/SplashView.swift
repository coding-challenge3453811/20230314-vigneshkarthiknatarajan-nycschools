//
//  SplashView.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import SwiftUI

/// Splash View
struct SplashView: View {
    var body: some View {
        ZStack {
            Color("splash_background")
            Text("navigation_title")
                .font(.system(size: 36.0))
                .foregroundColor(Color("text_color"))
        }
        .ignoresSafeArea()
    }
}
