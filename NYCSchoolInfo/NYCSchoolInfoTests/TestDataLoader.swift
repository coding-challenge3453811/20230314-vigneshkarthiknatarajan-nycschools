//
//  JSONLoader.swift
//  NYCSchoolInfoTests
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import Foundation

enum TestDataLoaderError: Error {
    case serializationError(error: Error)
    case fileNotFoundAtBundlePath
}

class TestDataLoader {
    static func loadObjectFromFile<T: Decodable>(
        objectType: T.Type,
        fileName: String,
        fileExtension: String = "json") throws -> T {
            if let bundle = Bundle.currentTestBundle,
               let path = bundle.path(forResource: fileName, ofType: fileExtension) {
                let url = URL(fileURLWithPath: path)
                do {
                    let data = try Data(contentsOf: url)
                    return try JSONDecoder().decode(objectType, from: data)
                } catch(let err) {
                    throw TestDataLoaderError.serializationError(error: err)
                }
            }
            throw TestDataLoaderError.fileNotFoundAtBundlePath
        }
}

extension Bundle {
    public static var currentTestBundle: Bundle? {
        allBundles.first { $0.bundlePath.hasSuffix(".xctest")}
    }
}
